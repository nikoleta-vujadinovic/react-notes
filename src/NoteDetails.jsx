import React from "react"
import { withRouter } from "react-router-dom"

export default withRouter(NoteDetails)

function NoteDetails(props) {
  const note = props.notesList.find(
    (notesListItem) => notesListItem.id === props.match.params.id
  )
  console.log(props)
  if (!note) {
    return <p>No note found.</p>
  }
  return (
    <div>
      <h3>{note.title}</h3>
      <p>{note.description}</p>
    </div>
  )
}
