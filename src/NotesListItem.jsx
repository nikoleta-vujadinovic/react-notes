import React from "react"
import { Link } from "react-router-dom"

export default function NotesListItem(props) {
  return (
    <li>
      <Link to={`/notes/${props.note.id}`}>{props.note.title}</Link>
    </li>
  )
}
