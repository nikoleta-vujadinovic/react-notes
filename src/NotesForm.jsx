import React from "react"
import { v4 as uuidv4 } from "uuid"
export default class NotesForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      title: "",
      description: ""
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.onReset = this.onReset.bind(this)
    this.onChangeTitle = this.onChangeTitle.bind(this)
    this.onChangeDescription = this.onChangeDescription.bind(this)
  }
  onSubmit(event) {
    event.preventDefault()
    this.props.onSubmit({
      title: this.state.title,
      description: this.state.description,
      id: uuidv4()
    })
    this.onReset()
  }
  onReset() {
    this.setState({
      title: "",
      description: ""
    })
  }
  onChangeTitle(event) {
    this.setState({
      title: event.target.value
    })
  }

  onChangeDescription(event) {
    this.setState({
      description: event.target.value
    })
  }
  render() {
    return (
      <form>
        <label htmlFor="title">Title</label>
        <input
          type="text"
          placeholder="Title"
          id="title"
          onChange={this.onChangeTitle}
          value={this.state.title}
        />
        <label htmlFor="description">Description</label>
        <input
          type="text"
          placeholder="Description"
          id="description"
          onChange={this.onChangeDescription}
          value={this.state.description}
        />
        <button onClick={this.onSubmit} type="submit">
          Submit
        </button>
        <button onClick={this.onReset} type="reset">
          Reset
        </button>
      </form>
    )
  }
}
