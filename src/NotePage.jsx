import React from "react"
import NoteDetails from "./NoteDetails"

export default class NotePage extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    console.log(this.props)

    return (
      <React.Fragment>
        <NoteDetails notesList={this.props.notesList} />
      </React.Fragment>
    )
  }
}
