import React from "react"
import NotesForm from "./NotesForm"
import NotesList from "./NotesList"

export default class NotesListPage extends React.Component {
  render() {
    return (
      <React.Fragment>
        <NotesForm onSubmit={this.props.onSubmit} />
        <NotesList notesList={this.props.notesList} />
      </React.Fragment>
    )
  }
}
