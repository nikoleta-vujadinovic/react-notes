import React, { Component } from "react"
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"
import "./App.css"
import HomePage from "./HomePage"
import NotesListPage from "./NotesListPage"
import NotePage from "./NotePage"
export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      notesList: []
    }
    this.onSubmit = this.onSubmit.bind(this)
  }
  onSubmit(note) {
    this.setState((state) => {
      return {
        notesList: [...state.notesList, note]
      }
    })
  }
  render() {
    return (
      <Router>
        <div>
          <Switch>
            <Route path="/" exact>
              <HomePage />
            </Route>
            <Route path="/notes" exact>
              <NotesListPage
                onSubmit={this.onSubmit}
                notesList={this.state.notesList}
              />
            </Route>
            <Route path="/notes/:id">
              <NotePage notesList={this.state.notesList} />
            </Route>
          </Switch>
        </div>
      </Router>
    )
  }
}
