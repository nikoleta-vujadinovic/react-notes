import React from "react"
import NotesListItem from "./NotesListItem"
export default function NotesList(props) {
  return (
    <ul>
      {props.notesList.map((note) => {
        return <NotesListItem key={note.id} note={note} />
      })}
    </ul>
  )
}
